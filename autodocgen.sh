#!/bin/bash 
# set -x #echo on

# This file will pull the latest doc template, script, and resources and build 
# the documentation
# Note that this assumes that there is a docs folder in the same location as the 
# script

# set variables
l_script=$(readlink -f $0)
l_script_path=`dirname $l_script`

l_output_path=$l_script_path/output
l_doc_path=$l_script_path/docs
l_repo=git@gitlab.com:slightlywolf/docgen.git
l_docgen_path=$l_script_path/docgen

l_final_template_path=$l_doc_path/default_template.tex
l_final_img_path=$l_doc_path/docgenimgs
l_final_script_path=$l_doc_path/docgen.py

# make the output path
mkdir -p $l_output_path

# clone the remote repo
git clone --depth 1 $l_repo

# move the files
cp $l_docgen_path/default_template.tex $l_final_template_path
cp -r $l_docgen_path/docgenimgs $l_final_img_path
cp $l_docgen_path/docgen.py $l_final_script_path

# move into the docs directory
cd $l_doc_path

# build the documentation
python3 $l_final_script_path -f $l_doc_path -t $l_final_template_path -o $l_output_path

# move the script, and images, and template to the docs folder
rm -rf $l_final_img_path
rm -rf $l_docgen_path
rm $l_final_script_path
rm $l_final_template_path

cd $l_script_path
