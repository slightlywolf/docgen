"""
This project is designed to assist in automatically generating documentation
for projects
"""

from argparse import ArgumentParser
import os
from pathlib import Path

# Constants
__DEFAULT_TEMPLATE = 'default_template.tex'

def main(args):
    for file in args.files:
        print(f'PROCESSING {file}...')
        output_path: str = generate_output_name(args.output, file)
        print('\tOutput path: ', output_path)
        cmd: str = generate_pandoc_command(args.template, output_path, file)
        print('\tCommand: ', cmd)
        run_cmd(cmd)

    print("Done!")


def run_cmd(cmd):
    os.system(cmd)
    pass


def generate_output_name(output_dir: str, md_file_path: str) -> str:
    basename: str = Path(md_file_path).stem
    return f'{output_dir}/{basename}.pdf'


def generate_pandoc_command(
        template: str, output_path: str, input_path: str) -> str:

    cmd: str = f'pandoc --template {template} \
--listings \
-f markdown \
-t latex \
-o {output_path} \
{input_path} '
    return cmd


def ensure_output_dir_exists(path: str):
    if not os.path.exists(path):
        os.makedirs(path)


def process_args(args):
    if args.output:
        ensure_output_dir_exists(args.output)
        if not os.path.isdir(args.output):
            raise Exception('Output path is not a directory')

    if args.template:
        if os.path.isdir(args.template):
            template_path = os.path.join(args.template, __DEFAULT_TEMPLATE)
            if os.path.exists(template_path):
                args.template = template_path
            else:
                raise Exception('Template path is not a file')

        if not os.path.isfile(args.template):
            raise Exception('Template path is not a file')

    if args.files is None:
        # if no args, just get the files from the current directory
        args.files = ['.']
    args.files = get_available_files(args.files)

    print("ARGUMENTS:")
    print("Input Files: ")
    for file in args.files:
        print("\t", file)
    print("Output Directory: ", args.output)
    print("Template: ", args.template)

    return args


def get_available_files(paths: list[str]):
    all_md_files: list[str] = []

    for path in paths:
        if os.path.isdir(path):
            for root, _, files in os.walk(path):
                for file in files:
                    if file.endswith('.md'):
                        all_md_files.append(os.path.join(root, file))

        else:
            if path.endswith('.md'):
                all_md_files.append(path)

    return all_md_files


def set_up_args():
    args: ArgumentParser = ArgumentParser(
        prog='raddocgen',
        description='Generate documentation from markdown files',
        epilog='Copyright 2024 Radlink Pty Ltd')

    args.add_argument('-o', '--output',
                      help='Output directory',
                      required=False,
                      default='./output')

    args.add_argument('-t', '--template',
                      help='Pandoc based latex template',
                      required=False,
                      default=f'./{__DEFAULT_TEMPLATE}')

    args.add_argument('-f', '--files',
                      nargs='+',
                      help='Markdown files or directories to be processed',
                      required=False)

    return args.parse_args()


# ------------------------------------------------------------------------------------
if __name__ == "__main__":
    try:
        args = set_up_args()
        args = process_args(args)
        main(args)
    except Exception as e:
        print(e)
# ------------------------------------------------------------------------------------

