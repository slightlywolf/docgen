rem This file will pull the latest doc template, script, and resources and build 
rem the documentation
rem Note that this assumes that there is a docs folder in the same location as the 
rem script

rem make a directory for the files to be pulled to 
set l_script_path=%~dp0

set l_output_path=%l_script_path%\output
set l_doc_path=%l_script_path%\docs
set l_repo=git@gitlab.com:slightlywolf/docgen.git
set l_docgen_path=%l_script_path%\docgen

set l_final_template_path=%l_doc_path%\default_template.tex
set l_final_img_path=%l_doc_path%\docgenimgs
set l_final_script_path=%l_doc_path%\docgen.py

mkdir %output_path%

rem clone the remote repo
git clone --depth 1 %l_repo%

rem move the script, and images, and template to the docs folder
move /Y %l_docgen_path%\default_template.tex %l_final_template_path%
move /Y %l_docgen_path%\docgenimgs %l_final_img_path%
move /Y %l_docgen_path%\docgen.py %l_final_script_path%

rem go into the docs folder, this is important to resolve the pathing for both
rem the latex template and the markdown files
cd %l_doc_path%

rem build the documentation
python %l_final_script_path% -f %l_doc_path% -t %l_final_template_path% -o %l_output_path%

rem move the script, and images, and template to the docs folder
rmdir /S /Q %l_final_img_path%
rmdir /S /Q %l_docgen_path%
del %l_final_script_path%
del %l_final_template_path%

cd %l_script_path%
