---
title: "Generating Documentation with raddocgen"
author: [Nathan Harmer]
date: \today
toc: true
numbersections: true
---

# raddocgen

This project is designed to assist in automatically generating documentation from
markdown files.

## Dependencies

### Python

Python should not have any dependencies, except python itself


### Latex/Pandoc 

Install either TeXLive or MikTeX along with their complete package
repositories. Pandoc recommends MikTeX.

pandoc can be installed from https://pandoc.org/installing.html


## Usage

```bash
raddocgen -f <input_files/input_directory> -o <output_directory> -t <template>
```

If ```-o``` is not specified, then output directory will default to ```.\output\```,
the folder will be created if it does not exist.

If an input_file/directory is not specified, then it will default to using a 
```.\docs\``` folder. Input directories will be scanned for files ending in .md .
If two input files have the same name, then the name of the folder they are located in
will be prepended to the name in the output folder. i.e. <folder-name>-<file-name>.pdf

If a template is not specified, then it will default to using 
```.\radlink_default.tex``` template in the same directory. If the template could not
be found then the application will exit.

## Footguns

Pathing can be a definite issue. To make sure there are absolutely no pathing issues,
please place the three exports from svn into the folder that contains the markdown 
files and delete them after.

## Usage in projects

This project is stored using ```SVN```. As a part of the build process for a project,
pull the raddocgen.py,radlink_default.tex template and the res directory from the repository. 

``` powershell
svn export https://rev-svn.radlink.local/svn/RadDocGen/raddocimgs .\raddocimgs --force
svn export https://rev-svn.radlink.local/svn/RadDocGen/raddocgen.py .\raddocgen.py --force
svn export https://rev-svn.radlink.local/svn/RadDocGen/radlink_default.tex .\radlink_default.tex --force
```

Then run the application.

``` powershell
python .\raddocgen.py -f . -o c:\proj\output -t c:\proj\radlink_default.tex
``` 

This will generate the documentation in ```c:\proj\output```

From here, do whatever you need with the documentation...


## autodocbuild

The autodocbuild.bat batch script can be found in this repository.

``` bat
rem This file will pull the latest doc template, script, and resources and build 
rem the documentation
rem Note that this assumes that there is a docs folder in the same location as the 
rem script

rem make a directory for the files to be pulled to 
mkdir %~dp0\output

rem go into the docs folder, this is important to resolve the pathing for both
rem the latex template and the markdown files
cd %~dp0\docs\

rem pull the three required files
svn export https://rev-svn.radlink.local/svn/RadDocGen/raddocimgs .\raddocimgs --force
svn export https://rev-svn.radlink.local/svn/RadDocGen/raddocgen.py .\raddocgen.py --force
svn export https://rev-svn.radlink.local/svn/RadDocGen/radlink_default.tex .\radlink_default.tex --force

rem build the documentation
python %~dp0\docs\raddocgen.py -f %~dp0\docs\ -t %~dp0\docs\radlink_default.tex -o %~dp0\output

rem clean up the files
rmdir /S /Q %~dp0\docs\raddocimgs
del %~dp0\docs\raddocgen.py
del %~dp0\docs\radlink_default.tex

cd %~dp0

...
```

To use this script, place it in the root folder of your project.

This script assumes all documentation will be placed in a docs folder.

After running the script an output folder will be generated with the built
documentation.


## Formatting

***TODO***

## Build server

If the build server is failing to generate the documentation, then most probably
the required latex package is not installed. The build server relies on
TeXLive as MikTeX does not support Windows  8.

Install TeXLive packages using the following command.

``` powershell
tlmgr install <package>
```

